#!/usr/bin/env python3

import re
import sys
import os.path
import subprocess

from collections import namedtuple

Subtest = namedtuple("Subtest", "name description")

def get_testlist(path):
    "read binaries' names from test-list.txt"
    with open(path, 'r') as f:
        assert(f.readline() == "TESTLIST\n")
        tests = f.readline().strip().split(" ")
        assert(f.readline() == "END TESTLIST\n")

    return tests

def get_subtests(testdir, test):
    "execute test and get subtests with their descriptions via --describe"
    output = []
    full_test_path = os.path.join(testdir, test)
    proc = subprocess.run([full_test_path, "--describe"], stdout=subprocess.PIPE)
    description = ""
    current_subtest = None

    for line in proc.stdout.decode().splitlines():
        if line.startswith("SUB "):
            output += [Subtest(current_subtest, description)]
            description = ""
            current_subtest = line.split(' ')[1]
        else:
            description += line

    output += [Subtest(current_subtest, description)]

    return output

def get_already_undocumented(path):
    with open(path, 'r') as f:
        undocumented = f.readlines()

    return [ x.strip() for x in undocumented ]

def main():
    undocumented_file = sys.argv[1]
    testlist_file     = sys.argv[2]
    testdir           = os.path.abspath(os.path.dirname(testlist_file))

    newly_undocumented = []
    already_undocumented = get_already_undocumented(undocumented_file)

    tests = get_testlist(testlist_file)

    for test in tests:
        subtests = get_subtests(testdir, test)

        for name, description in subtests:
            if name is None: # top level description
                if not description: # is empty
                    if test not in already_undocumented:
                        newly_undocumented += [testname]
                continue # and skip because it's not a subtest

            if "NO DOCUMENTATION!" in description:
                testname = "{}@{}".format(test, name)
                if testname not in already_undocumented:
                    newly_undocumented += [testname]

    if newly_undocumented:
        print("New undocumented tests!")
        for testname in newly_undocumented:
            print("  {}".format(testname))
        print("\nPlease refer to CONTRIBUTING.md")
        sys.exit(1)

main()
